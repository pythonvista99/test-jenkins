#!/bin/sh
import json
import requests
import os
import sys

def get_merge_url_by_hash(BITBUCKET_USERNAME, REPO_Name, BITBUCKET_AUTH_TOKEN, hash):
    req = requests.get('https://api.bitbucket.org/2.0/repositories/' + BITBUCKET_USERNAME + '/' + REPO_Name + '/pullrequests', headers={'Authorization': 'Basic ' + BITBUCKET_AUTH_TOKEN})

    print('=====get_merge_url_by_hash====')
    print('\n Response Status Code===='+str(req.status_code))
    json_res = json.loads(req.content)
    print('Node Hash==='+hash) # Commit ID
    for val in json_res['values']:
        if hash in val['title']:
            print('Merge URL=====' + val['links']['merge']['href'])
            print('\n***************************************************\n')
            return val['links']['merge']['href']

    return None

def merge_pull_request(BITBUCKET_USERNAME, REPO_Name, BITBUCKET_AUTH_TOKEN, source_branch, dest_branch, hash):
    merge_url = get_merge_url_by_hash(BITBUCKET_USERNAME, REPO_Name, BITBUCKET_AUTH_TOKEN, hash)

    print('=====merge_pull_request===='+merge_url)

    req = requests.post(merge_url, headers={'Authorization': 'Basic '+BITBUCKET_AUTH_TOKEN}, json={
            "type": "MERGE",
            "close_source_branch": "true",
            "message": "Merge branch >> " + source_branch + " -> " + dest_branch + " -> " + hash
    })

    print('=====merge_pull_request====')
    print('\n Response Status Code===='+str(req.status_code))
    print('\n***************************************************\n')
    return str(req.content)
	
if __name__ == "__main__":
	BITBUCKET_USERNAME = sys.argv[1]
	REPO_Name = sys.argv[2]
	BITBUCKET_AUTH_TOKEN = sys.argv[3]
	source_branch = sys.argv[4]
	dest_branch = sys.argv[5]
	hash = sys.argv[6]
	
	print('BITBUCKET_USERNAME===='+BITBUCKET_USERNAME)
	print('REPO_Name===='+REPO_Name)
	print('BITBUCKET_AUTH_TOKEN===='+BITBUCKET_AUTH_TOKEN)
	print('source_branch===='+source_branch)
	print('dest_branch===='+dest_branch)
	print('hash===='+hash)
	
	merge_pull_request(BITBUCKET_USERNAME, REPO_Name, BITBUCKET_AUTH_TOKEN, source_branch, dest_branch, hash)